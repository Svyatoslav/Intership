package controllers

import play.api.mvc._
import play.api.libs.json.Json._
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, JsValue ,Reads}


/**
  * Created by palkoleg on 04.04.16.
  */
object App2 extends Controller {
  implicit val People: Reads[(Int, String,Int,  Int)] = (
    (JsPath \ "ID").read[Int] and
    (JsPath \ "Name").read[String] and
      (JsPath \ "Age").read[Int] and
      (JsPath \ "Numder").read[Int]
    ).tupled

  def sourse = Action { implicit request =>

    //val phones = Map([{"ID"->"1","Name"->"Oleg", "Age"->"18",Number"->"+3806785975"} , {"ID"->"2", "Name"->"Vitaliy", "age"->"35","Number"->"+38096192457"}, {"ID"->"3", "Name"->"Oksana", "age"->"22","Number"->"+380939688657"},{"ID"->\"4", "Name"->"Max", "age"->"40","Number"->"+380957856932"}, {"ID"->\"5", "Name"->"Ivan", "age"->"16","Number"->"+380973468541}])
    val phones : JsValue = Json.arr(
      Json.obj
      ("ID" -> "1",
        "Name" -> "Oleg",
        "Age" -> "18",
        "Number" -> "3806785975"),
      Json.obj
      ("ID" -> "2",
        "Name" -> "Vitaliy",
        "age" -> "35",
        "Number" -> "38096192457"),
      Json.obj
      ("ID" -> "3",
        "Name" -> "Oksana",
        "age" -> "22",
        "Number" -> "380939688657"),
      Json.obj
      ("ID" -> "5",
        "Name" -> "Ivan",
        "age" -> "16",
        "Number" -> "380973468541"),
      Json obj("ID" -> "4",
        "Name" -> "Max",
        "age" -> "40",
        "Number" -> "380957856932"))
    Ok(toJson(phones))
  }

  def sourse2 = Action(parse.json) { implicit request =>

    request.body.validate(People).map {
      case (id,name, age, number) => Ok(toJson(Map(name -> number)))
    }.getOrElse {
      BadRequest(toJson(Map("error" -> "1")))
    }
  }




}
