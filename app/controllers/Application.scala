package controllers

import play.api.mvc._
import play.api.libs.json.Json._
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}


object Application extends Controller {

  implicit val placeReads: Reads[(String, String)] = (
    (JsPath \ "name").read[String] and
      (JsPath \ "lastname").read[String]
    ).tupled

  def index = Action { implicit request =>
    Ok(views.html.index.render())
  }

  def ind = Action(parse.json) { implicit request =>

    request.body.validate(placeReads).map {
      case (name, lastname) => Ok(toJson(Map(name -> lastname)))
    }.getOrElse {
      BadRequest(toJson(Map("error" -> "1")))
    }
 }

}
