/**
 * Created by svyatoslav on 04.04.16.
 */
angular.module('app')
.service('telephoneService', ['$resource', function($resource) {
	return $resource('/people');
}]);