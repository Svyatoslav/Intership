/**
 * Created by svyatoslav on 04.04.16.
 */

angular.module('app').config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: '/assets/views/telephones.html',
        controller: 'telephones'
    });
}]);
